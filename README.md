# metru vue js task with firebase

> A vue2-Task with firebase project

## Config
At -> src/App.vue     [firebase console](https://console.firebase.google.com/)
``` bash
let config = {
    //firebase console
}
```
## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

